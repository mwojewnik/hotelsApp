import React, {useState} from 'react';
import {CardActions, Paper, Rating, Card} from '@mui/material'
import {Buttons} from "../components/Buttons";


export const Header = () => {

    return (
        <Card sx={{my: 3, height: '400px'}} elevation={5}>
            <CardActions sx={{p: 0, justifyContent: 'center'}}>
                <img style={{height: '400px', width: '100%', position: 'relative'}}
                     src='https://nawalizkach.com.pl/wp-content/uploads/2018/01/podroz_samolot_dookola1-1140x729.jpg'/>
                <Card sx={{position: 'absolute', mt: 48, }}>
                <Buttons />
                </Card>

            </CardActions >
        </Card>
    )
}
