import {CardContent, CardHeader, Paper, Rating} from "@mui/material";
import {Rooms} from "../components/Rooms";
import React, {useContext} from "react";
import {ImageSlider} from "../components/ImageSlider";



export const HotelCard = ({item}: any) => {

    const imageStyle={
        borderRadius: '10px',
        width: '300px',
        height: '250px',
        marginLeft: 32,
        marginRight: 32
    }

    return (
        <Paper key={item.id} sx={{my: 6}} elevation={5}>

            <CardHeader
                className="header"
                sx={{width: '100%', pt: 3}}
                avatar={<ImageSlider style={imageStyle} data={item.images}/>}
                action={<Rating name="read-only"  value={parseInt(item.starRating)} readOnly/>}
                title={<h1>{item.name}</h1>}
                subheader={
                    <>
                        <h2>{item.address1}</h2>
                        <h2>{item.address2}</h2>
                    </>}
            />
            <CardContent sx={{minHeight: '500px'}}>
                <Rooms hotelId={item.id}/>
            </CardContent>
        </Paper>
    )
}
