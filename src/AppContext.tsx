import React, {createContext, useState} from 'react';

export const AppContext = createContext({});

const AppProvider = ({children}: any) => {
    const [starsQuery, setStarsQuery] = useState(2);
    const [adultsQuery, setAdultsQuery] = useState(0);
    const [childQuery, setChildQuery] = useState(0);

    const starsQueryHandler = (value: any) => setStarsQuery(value);
    const adultsQueryHandler = (value: any) => setAdultsQuery(value);
    const childQueryHandler = (value: any) => setChildQuery(value);


    return (
        <AppContext.Provider
            value={{
                starsQuery,
                starsQueryHandler,
                adultsQuery,
                adultsQueryHandler,
                childQuery,
                childQueryHandler
            }}>
            {children}
        </AppContext.Provider>
    )
}

export default AppProvider
