import React, {useState} from 'react';
import {Container, Grid} from '@mui/material'

import {HotelCards} from "./components/HotelCards";
import {Header} from "./views/Header";
import AppProvider from './AppContext'

const App = () => {

    return (
        <Container>
            <AppProvider>
                <Header/>
                <HotelCards/>
            </AppProvider>
        </Container>
    );
}

export default App;
