import React, {useContext, useEffect, useState} from 'react';
import axios from "axios";
import {HotelCard} from '../views/HotelCard'

import {AppContext} from '../AppContext'

export const HotelCards = () => {

    const {starsQuery}: any = useContext(AppContext)
    const [hotels, setHotels] = useState<any[]>([])


    useEffect(() => {

        axios.get('https://obmng.dbm.guestline.net/api/hotels?collection-id=OBMNG')
            .then(res => setHotels(res.data))

    }, [])

    const filtered = hotels.filter(hotel => {
        if (hotel.starRating >= starsQuery) {
            return hotel
        }
    })


    return (
        <div>
            {filtered?.map((item: any) => (
                <HotelCard key={item.id} item={item}/>
            ))}

        </div>
    )
}



