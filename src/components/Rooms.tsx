import React, {useContext, useEffect, useState} from "react";
import axios from "axios";
import {Container, Divider, Grid, List, ListItem} from "@mui/material";
import {Room} from './Room'

import {AppContext} from '../AppContext'
import {ImageSlider} from "./ImageSlider";

export const Rooms = ({hotelId}: any) => {

    const {adultsQuery}: any = useContext(AppContext)
    const {childQuery}: any = useContext(AppContext)
    const [rooms, setRooms] = useState<any[]>([])



    useEffect(() => {
        axios.get('https://obmng.dbm.guestline.net/api/roomRates/OBMNG/' + hotelId)
            .then(res => setRooms(res.data.rooms))
    }, [])




    const filtered = rooms.filter(room => {
        if (room?.occupancy.maxAdults >= adultsQuery && room?.occupancy.maxChildren >= childQuery ) {
            return room
        }

    })



    return (
        <Container maxWidth={false}>
            <List sx={{
                width: "100%",
                maxWidth: '100%',
                position: "relative",
                overflow: "auto",
                maxHeight: 500,
            }}>
                {filtered?.map((room: any) => (
                    <Room key={room.id} room={room}/>
                ))}

            </List>

        </Container>
    )
}
