import React, {useState} from 'react';
import {Divider, Grid, ListItem, Collapse, Button} from "@mui/material";
import {ImageSlider} from './ImageSlider'

export const Room = ({room}:any) => {
    const [open, setOpen] = useState(false)

    return (
        <React.Fragment  >
            <ListItem >
                <Grid container spacing={2}>
                    <Grid item lg={12} sx={{py: 3, display: 'flex'}}>
                        <Grid item lg={4}>
                            <h4>{room.name}</h4>
                            <p>{`Adults: ${room.occupancy.maxAdults}`}</p>
                            <p>{`Children: ${room.occupancy.maxChildren}`}</p>
                        </Grid>
                        <Grid item lg={8}>
                            <p>{room.longDescription}</p>
                        </Grid>
                    </Grid>
                    {room?.images?.length > 0 && <Grid item lg={12} sx={{textAlign: 'center'}}>
                        <Button onClick={() => setOpen(!open)}>Watch Photos!</Button>
                        <Collapse in={open} timeout="auto" unmountOnExit>
                            <div style={{justifyContent: 'center', textAlign: 'center'}}>
                                {room?.images?.map((image: any, index: any) => (
                                    <img key={index} style={{marginRight: 2, marginLeft: 2}} src={image.url}/>
                                ))}
                            </div>
                        </Collapse>
                    </Grid>}
                </Grid>
            </ListItem>
            <Divider/>
        </React.Fragment>
    )
}
