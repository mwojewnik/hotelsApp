import React, {useState} from 'react';
import {IconButton} from '@mui/material';
import * as Icons from "@mui/icons-material";

export const ImageSlider = ({data, style}: any) => {

    const [current, setCurrent] = useState(0);
    const length = data.length


    const nextSlide = () => {
        setCurrent(current === length -1 ? 0 : current + 1)
    }

    const prevSlide = () => {
        setCurrent(current === 0 ? length - 1 : current - 1)
    }

    if (!Array.isArray(data) || length <= 0) {
        return null
    }

    return (
        <div style={{
            position: 'relative',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',


        }}>
            {length > 1 ?
                <>
                    <IconButton
                        onClick={prevSlide}
                        sx={{
                            position: 'absolute',
                            zIndex: 10,
                            cursor: 'pointer',
                            left: '32px'
                        }}>
                        <Icons.NavigateBefore sx={{fontSize: '60px', color: 'white'}}/>
                    </IconButton>
                    <IconButton
                        onClick={nextSlide}
                        sx={{
                            position: 'absolute',
                            zIndex: 10,
                            cursor: 'pointer',
                            right: '32px'
                        }}>
                        <Icons.NavigateNext sx={{fontSize: '60px', color: 'white'}}/>
                    </IconButton> </> : null
            }
            {data?.map((slide: any, index: any) => (
                <div key={index}>
                    {index === current && (
                        <img style={style} src={slide.url}
                             alt="photo"/>)}
                </div>


            ))}
        </div>
    )
}

