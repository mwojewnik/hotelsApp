import React, {useState, useContext} from 'react';
import {Rating, Paper, IconButton, Button, CardActions} from "@mui/material";
import * as Icons from "@mui/icons-material";

import {AppContext} from '../AppContext'

export const Buttons = () => {

    const {starsQuery}: any = useContext(AppContext)
    const {starsQueryHandler}: any = useContext(AppContext)

    const {adultsQuery}: any = useContext(AppContext)
    const {adultsQueryHandler}: any = useContext(AppContext)

    const {childQuery}: any = useContext(AppContext)
    const {childQueryHandler}: any = useContext(AppContext)





    return (
        <Paper style={{textAlign: 'center',}}>
            <CardActions>
                <Rating value={starsQuery} onChange={(event, newValue: any) => starsQueryHandler(newValue)}
                        sx={{fontSize: '30px', mr: 3}}/>
                Adults:
                <IconButton onClick={(e) => adultsQueryHandler(adultsQuery + 1)} title='Ustaw'>
                    <Icons.Add/>
                </IconButton>
                {adultsQuery}
                <IconButton onClick={(e) => adultsQueryHandler(adultsQuery - 1)} title='Ustaw'>
                    <Icons.Remove/>
                </IconButton>
                Children:
                <IconButton onClick={(e) => childQueryHandler(childQuery + 1)} title='Ustaw'>
                    <Icons.Add/>
                </IconButton>
                {childQuery}
                <IconButton onClick={(e) => childQueryHandler(childQuery - 1)} title='Ustaw'>
                    <Icons.Remove/>
                </IconButton>
            </CardActions>

        </Paper>
    )
}
